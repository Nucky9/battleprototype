﻿using UnityEngine;

[System.Serializable]
public class MissileLauncher
{
    public bool active;
    public int numberOfMissiles;
    public Missile.MissileStats missileStats;
}
