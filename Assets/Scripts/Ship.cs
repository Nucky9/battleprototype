﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    public enum ShipRole
    {
        Carrier,
        PointDefense,
        Assault,
        Missile,
        Escort
    }

    public enum ShipClass
    {
        Destroyer,
        LightCruiser,
        HeavyCruiser,
        BattleShip
    }

    public int shipID;
    public bool active;
    public bool damaged;
    public Transform shipTransform;

    public float speed;
    public float shields;
    public float hull;
    public ShipRole shipRole;
    public ShipClass shipClass;
    public Collider2D col;

    public MissileLauncher launcher;

    public void InitializeNewShip()
    {
        shipTransform = transform;
    }

    public void InitializeShip(float _speed, float _shields, float _hull, ShipRole role, ShipClass _shipClass)
    {
        speed = _speed;
        shields = _shields;
        hull = _hull;
        shipRole = role;
        shipClass = _shipClass;
    }

    public void TakeDamage(float damage)
    {
        if (shields > 0)
        {
            shields -= damage;
            if (shields < 0)
            {
                shields = 0;
            }
        }
        else
        {
            hull -= damage;
            if (hull < 0)
            {
                ShipDestroyed();
            }
        }
    }

    void ShipDestroyed()
    {
        print($"You sunk my {shipClass}!");
    }
}
