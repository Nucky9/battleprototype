﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileManager : Singleton<MissileManager>
{
    enum UpdatePhase
    {
        Movement,
        PDS,
        Impacts
    }

    public GameObject missile;
    public int startingMissilePoolSize;
    public Transform missilePool;

    int currentPoolSize;
    Missile[] missiles;
    UpdatePhase currentPhase;
    int numPhases;


    public void InitializePoolManager()
    {
        currentPoolSize = startingMissilePoolSize;
        InitializeMissilePool();
        numPhases = Enum.GetValues(typeof(UpdatePhase)).Length; // will only work if auto enum
        currentPhase = UpdatePhase.Movement;
    }

    // schedule updates as follows:
    // Frame 1: Update missile movement
    // Frame 2: Check for PDS
    // Frame 3: Check for Impacts
    // Repeat from Frame 1
    public void UpdateMissileManager()
    {
        switch (currentPhase)
        {
            case UpdatePhase.Movement:
                MoveMissiles();
                break;
            case UpdatePhase.PDS:
                CheckPDS();
                break;
            case UpdatePhase.Impacts:
                CheckImpacts();
                break;
        }

        IncrementPhase();
    }

    void MoveMissiles()
    {
        for (int i = 0; i < missiles.Length; i++)
        {
            if (missiles[i].active && missiles[i].targetShip.active)
            {
                missiles[i].transform.Translate(missiles[i].targetDirection * missiles[i].missileStats.speed * numPhases * Time.deltaTime);
            }
            else
            {
                DestroyMissile(missiles[i]);
            }
        }
    }

    void DestroyMissile(Missile missileToDestroy)
    {
        missileToDestroy.active = false;
        missileToDestroy.transform.position = missilePool.transform.position;
        missileToDestroy.gameObject.SetActive(false);
    }

    void CheckPDS()
    {

    }

    void CheckImpacts()
    {
        for (int i = 0; i < missiles.Length; i++)
        {
            if (missiles[i].active && missiles[i].targetShip.active)
            {
                if (missiles[i].col.bounds.Intersects(missiles[i].targetShip.col.bounds))
                {
                    BattleManager.Instance.DamageShip(missiles[i].missileStats.damage, missiles[i].targetShip);
                    DestroyMissile(missiles[i]);
                }
            }
            else
            {
                DestroyMissile(missiles[i]);
            }
        }
    }

    void IncrementPhase()
    {
        currentPhase = (UpdatePhase)(((int)currentPhase + 1) % numPhases);
    }

    void InitializeMissilePool()
    {
        missiles = new Missile[currentPoolSize];

        for (int i = 0; i < currentPoolSize; i++)
        {
            GameObject newMissile = Instantiate(missile, missilePool);
            newMissile.SetActive(false);
            missiles[i] = newMissile.GetComponent<Missile>();
        }
    }

    public Missile GetMissileFromPool()
    {
        for (int i = 0; i < currentPoolSize; i++)
        {
            if (!missiles[i].active)
            {
                return missiles[i];
            }
        }
        int nextIndex = currentPoolSize;
        currentPoolSize *= 2;
        Array.Resize(ref missiles, currentPoolSize);

        for (int i = nextIndex; i < currentPoolSize; i++)
        {
            GameObject newMissile = Instantiate(missile, missilePool);
            missiles[i] = newMissile.GetComponent<Missile>();
        }
        return missiles[nextIndex];
    }
}
