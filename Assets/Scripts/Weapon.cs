﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public enum WeaponRange
    {
        Close,
        Medium,
        Long
    }
    
    public WeaponRange range;
    public float damage;
    public float fireRate;

}

