﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fleet : MonoBehaviour
{
    public enum Formation
    {
        Mixed,
        Picket,
        Assault
    }

    public List<GameObject> shipsInFleet;
    public List<GameObject> frontSlots;
    public List<GameObject> middleSlots;
    public List<GameObject> rearSlots;

    public int maxFleetSize;
    public int maxRowSize;

    private void Start()
    {
        frontSlots = new List<GameObject>();
        middleSlots= new List<GameObject>();
        rearSlots = new List<GameObject>();

        InitializeFleet();
    }

    void InitializeFleet()
    {

    }
}
