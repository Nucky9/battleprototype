﻿using UnityEngine;

public class Missile : MonoBehaviour
{ 
    [System.Serializable]
    public struct MissileStats
    {
        public float speed;
        public float damage;
        public float evasion;
    }

    public bool active;
    public MissileStats missileStats;
    public Ship targetShip;
    public Vector3 targetDirection;
    public CapsuleCollider2D col;
}
