﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missiles : MonoBehaviour
{

    public Transform[] missileTransforms;
    public GameObject upperBound;
    public GameObject lowerBound;

    Transform trans;

    float upperBoundY;
    float lowerBoundY;

    public float missileSpeed;
    public const int numFramesBetweenUpdates = 3;
    int framesSinceLastUpdate = 0;

    private void Start()
    {
        upperBoundY = upperBound.transform.position.y;
        lowerBoundY = lowerBound.transform.position.y;
        for (int i = 0; i < missileTransforms.Length; i++)
        {
            float randomY = Random.Range(lowerBoundY, upperBoundY);
            Vector3 newPosition = new Vector3(missileTransforms[i].position.x, randomY, 0f);
            missileTransforms[i].transform.position = newPosition;
        }
        trans = transform;
    }
    // Update is called once per frame
    void Update()
    {
        framesSinceLastUpdate++;
        if (framesSinceLastUpdate >= numFramesBetweenUpdates)
        {
            framesSinceLastUpdate = 0;
        }
        UpdateMissiles();
    }


    void UpdateMissiles()
    {
        UnityEngine.Profiling.Profiler.BeginSample("SomeUniqueName");

        for (int i = 0; i < missileTransforms.Length; i++)
        {
            if (missileTransforms[i].position.y > upperBoundY)
            {

                missileTransforms[i].position = new Vector3(missileTransforms[i].position.x, lowerBoundY, 0f);
            }
            else
            {
                missileTransforms[i].Translate(0, missileSpeed * numFramesBetweenUpdates, 0);
            }
        }
        UnityEngine.Profiling.Profiler.EndSample();

    }
}
