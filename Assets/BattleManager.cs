﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleManager : Singleton<BattleManager>
{
    public Ship[] enemyShips;
    public Ship[] alliedShips;
        
    void Start()
    {
        MissileManager.Instance.InitializePoolManager();
        for (int i = 0; i < alliedShips.Length; i++)
        {
            if (alliedShips[i].launcher.active)
            {
                for (int n = 1; n <= alliedShips[i].launcher.numberOfMissiles; n++)
                {
                    Missile newMissile = MissileManager.Instance.GetMissileFromPool();
                    newMissile.missileStats = alliedShips[i].launcher.missileStats;
                    newMissile.active = true;
                    newMissile.gameObject.SetActive(true);
                    newMissile.transform.position = alliedShips[i].transform.position;
                    newMissile.targetShip = enemyShips[0];
                    newMissile.targetDirection = (newMissile.targetShip.transform.position - newMissile.transform.position).normalized;
                }
            }
        }
    }

    private void Update()
    {
        MissileManager.Instance.UpdateMissileManager();
        CheckForDestroyedShips();
    }

    public void DamageShip(float damage, Ship targetShip)
    {
        targetShip.damaged = true;
        float damageAfterShields = damage;
        float damageToApplyToShields = targetShip.shields;
        damageAfterShields -= damageToApplyToShields;
        targetShip.shields -= damageToApplyToShields;
        targetShip.hull -= damageAfterShields;
    }

    void CheckForDestroyedShips()
    {
        for (int i = 0; i < enemyShips.Length; i++)
        {
            if (enemyShips[i].active && enemyShips[i].damaged)
            {
                enemyShips[i].damaged = false;
                if(enemyShips[i].hull <= 0)
                {
                    DestroyShip(enemyShips[i]);
                }
                else
                {
                    UpdateShipDisplay(enemyShips[i]);
                }
            }
        }
    }

    void DestroyShip(Ship shipToDestroy)
    {
        shipToDestroy.active = false;
        shipToDestroy.gameObject.SetActive(false);
    }

    void UpdateShipDisplay(Ship ship)
    {
        print($"Shields = {ship.shields}; Hull = {ship.hull}");
    }
}
